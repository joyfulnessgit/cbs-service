package service

import (
	"encoding/csv"
	"fmt"
	"os"
	"strconv"
	"time"
)

type ReportServ struct {
	Storage Storage
	Now     func() time.Time
}

func NewReportService() *ReportServ {
	return &ReportServ{App.Storage, func() time.Time { return time.Now().Truncate(time.Second) }}
}

func (rs *ReportServ) CreateUserReport(userID string) Error {
	cr := GetCurrentRound()
	u, err := rs.Storage.GetUser(userID, cr)
	if err != nil {
		return NewErrors(400, `report_createuserreport_getuser`, err.Error())
	}

	if u.IsDone == true {
		return nil
	}

	uaList, err := rs.Storage.GetUserAnswers(userID, u.Round)
	if err != nil {
		return NewErrors(400, `report_createuserreport_getuseranswer`, err.Error())
	}

	qs, err := GetQuestions(u.Round)
	if err != nil {
		return NewErrors(500, `report_createuserreport_getquestions`, err.Error())
	}

	if _, ok := qs[u.Department]; !ok {
		return NewErrors(404, `report_createuserreport_getquestions`, `ไม่พบคำถาม`)
	}

	if len(qs[u.Department].Questions) != len(uaList) {
		return NewErrors(404, `report_createuserreport_getquestions`, `กรุณาตอบคำถามให้ครบทุกข้อก่อนส่งคำตอบ`)
	}

	u.Functionals = make(map[string]*Functional)

	for _, q := range qs[u.Department].Questions {
		if u.Functionals[q.Functional] == nil {
			u.Functionals[q.Functional] = &Functional{}
			u.Functionals[q.Functional].Title = qs[u.Department].Functionals[q.Functional].Title
			u.Functionals[q.Functional].TotalScore = qs[u.Department].Functionals[q.Functional].TotalScore
		}
		if uaList[q.ID].UserAnswer == q.Answer {
			u.Score++
			u.Functionals[q.Functional].Score++
			u.Functionals[q.Functional].UserScore++
		}
	}

	//pass score
	ps := float32(len(qs[u.Department].Questions)) * 0.8

	if float32(u.Score) >= ps {
		u.IsPassed = true
	} else {
		u.IsPassed = false
	}
	u.FinishedTime = rs.Now()
	u.IsDone = true

	if err := rs.Storage.SaveUser(u); err != nil {
		return NewErrors(500, `report_createuserreport`, err.Error())
	}

	return nil
}

func (rs *ReportServ) GenerateReport() Error {
	cr := GetCurrentRound()
	if err := rs.Storage.ClearReport(cr); err != nil {
		return NewErrors(500, `report_generatereport`, err.Error())
	}

	uList, err := rs.Storage.GetAllUserDoneCurrentRound(cr)
	if err != nil {
		return NewErrors(500, `report_generatereport`, err.Error())
	}

	qs, aerr := GetQuestions(cr)
	if aerr != nil {
		return NewErrors(500, `report_generatereport`, aerr.Error())
	}

	drList := make(map[string]*DepartmentReport)
	brList := make(map[string]*BranchReport)
	prList := make(map[string]*PartReport)

	ar := AllReport{
		Round:  cr,
		Report: &Report{},
	}

	for _, u := range uList {
		if _, ok := drList[u.Department]; !ok {
			drList[u.Department] = &DepartmentReport{
				Department: u.Department,
				Round:      cr,
				Report: &Report{
					Functionals: make(map[string]*Functional),
				},
			}
			for m, n := range qs[u.Department].Functionals {
				drList[u.Department].Report.Functionals[m] = &Functional{
					Title:      n.Title,
					Score:      n.Score,
					UserScore:  n.UserScore,
					TotalScore: n.TotalScore,
				}
			}
		}

		bp := u.Part + u.Branch

		if _, ok := brList[bp]; !ok {
			brList[bp] = &BranchReport{
				Part:              u.Part,
				Branch:            u.Branch,
				Round:             cr,
				TotalUser:         0,
				DepartmentReports: make(map[string]*DepartmentReport),
			}
		}

		if _, ok := brList[bp].DepartmentReports[u.Department]; !ok {
			brList[bp].DepartmentReports[u.Department] = &DepartmentReport{
				Department: u.Department,
				Round:      cr,
				Report: &Report{
					Functionals: make(map[string]*Functional),
				},
			}

			for m, n := range qs[u.Department].Functionals {
				brList[bp].DepartmentReports[u.Department].Report.Functionals[m] = &Functional{
					Title:      n.Title,
					Score:      n.Score,
					UserScore:  n.UserScore,
					TotalScore: n.TotalScore,
				}
			}
		}

		if _, ok := prList[u.Part]; !ok {
			prList[u.Part] = &PartReport{
				Part:              u.Part,
				Round:             cr,
				TotalUser:         0,
				DepartmentReports: make(map[string]*DepartmentReport),
			}
		}

		if _, ok := prList[u.Part].DepartmentReports[u.Department]; !ok {
			prList[u.Part].DepartmentReports[u.Department] = &DepartmentReport{
				Department: u.Department,
				Round:      cr,
				Report: &Report{
					Functionals: make(map[string]*Functional),
				},
			}

			for m, n := range qs[u.Department].Functionals {
				prList[u.Part].DepartmentReports[u.Department].Report.Functionals[m] = &Functional{
					Title:      n.Title,
					Score:      n.Score,
					UserScore:  n.UserScore,
					TotalScore: n.TotalScore,
				}
			}
		}

		brList[bp].TotalUser++
		prList[u.Part].TotalUser++

		rs.generateDepartmentReport(u, drList[u.Department])
		rs.generateDepartmentReport(u, brList[bp].DepartmentReports[u.Department])
		rs.generateDepartmentReport(u, prList[u.Part].DepartmentReports[u.Department])
		rs.generateAllReport(u, &ar)
	}
	fmt.Println(`[Done] Generate Report`)

	ch := make(chan struct{}, 4)

	go func() {
		fmt.Println(`[Start] Save department report to db`)
		for _, dr := range drList {
			if err := rs.Storage.SaveDepartmentReport(dr); err != nil {
				fmt.Println(err.Error())

			}
		}
		ch <- struct{}{}
		fmt.Println(`[Done] Save department report to db`)
	}()

	go func() {
		fmt.Println(`[Start] Save branch report to db`)
		for _, br := range brList {
			if err := rs.Storage.SaveBranchReport(br); err != nil {
				fmt.Println(err.Error())
			}
		}
		ch <- struct{}{}
		fmt.Println(`[Done] Save branch report to db`)
	}()

	go func() {
		fmt.Println(`[Start] Save part report to db`)
		for _, pr := range prList {
			if err := rs.Storage.SavePartReport(pr); err != nil {
				fmt.Println(err.Error())
			}
		}
		ch <- struct{}{}
		fmt.Println(`[Done] Save part report to db`)
	}()

	go func() {
		fmt.Println(`[Start] Save all report to db`)
		if err := rs.Storage.SaveAllReport(&ar); err != nil {
			fmt.Println(err.Error())
		}
		ch <- struct{}{}
		fmt.Println(`[Done] Save all report to db`)
	}()

	<-ch
	<-ch
	<-ch
	<-ch

	return nil
}

func (rs *ReportServ) generateDepartmentReport(u *User, dr *DepartmentReport) {
	dr.Report.TotalUser++

	for k, v := range u.Functionals {
		if dr.Report.Functionals[k] == nil {
			dr.Report.Functionals[k] = &Functional{}
		}

		dr.Report.Functionals[k].TotalScore = v.TotalScore
		dr.Report.Functionals[k].UserScore += v.UserScore
		dr.Report.Functionals[k].Score = float32(dr.Report.Functionals[k].UserScore) / float32(dr.Report.TotalUser)
	}

	if dr.Report.TotalUser == 1 {
		dr.Report.MinScore = u.Score
	}
	if u.Score < dr.Report.MinScore && dr.Report.TotalUser > 1 {
		dr.Report.MinScore = u.Score
	}

	if u.Score > dr.Report.MaxScore {
		dr.Report.MaxScore = u.Score
	}

	dr.Report.UserScore += u.Score
	dr.Report.TotalScore += u.TotalScore
	dr.Report.MeanScore = float32(dr.Report.UserScore) / float32(dr.Report.TotalUser)

	if u.IsPassed {
		dr.Report.Passed++
	} else {
		dr.Report.Failed++
	}
}

func (rs *ReportServ) generateAllReport(u *User, ar *AllReport) {

	ar.Report.TotalUser++

	if ar.Report.TotalUser == 1 {
		ar.Report.MinScore = u.Score
	}
	if u.Score < ar.Report.MinScore && ar.Report.TotalUser > 1 {
		ar.Report.MinScore = u.Score
	}

	if u.Score > ar.Report.MaxScore {
		ar.Report.MaxScore = u.Score
	}

	ar.Report.UserScore += u.Score
	ar.Report.TotalScore += u.TotalScore
	ar.Report.MeanScore = float32(ar.Report.UserScore) / float32(ar.Report.TotalUser)

	if u.IsPassed {
		ar.Report.Passed++
	} else {
		ar.Report.Failed++
	}
}

func (rs *ReportServ) GetUserReport(userID string, r string) (*User, Error) {
	u, err := rs.Storage.GetUser(userID, r)
	if err != nil {
		return nil, NewErrors(500, `user_report`, err.Error())
	}

	return u, nil
}

func (rs *ReportServ) GetAllDepartmentReport(r string) (map[string]*DepartmentReport, Error) {
	drList, err := rs.Storage.GetDepartmentReports(r)
	if err != nil {
		return nil, NewErrors(500, `department_report`, err.Error())
	}

	return drList, nil
}

func (rs *ReportServ) GetBranchReport(p, b, r string) (*BranchReport, Error) {
	br, err := rs.Storage.GetBranchReport(p, b, r)
	if err != nil {
		return nil, NewErrors(500, `branch_report`, err.Error())
	}

	return br, nil
}

func (rs *ReportServ) GetPartReport(p string, r string) (*PartReport, Error) {
	pr, err := rs.Storage.GetPartReport(p, r)
	if err != nil {
		return nil, NewErrors(500, `part_report`, err.Error())
	}

	return pr, nil
}

func (rs *ReportServ) GetAllBranchReport(r string) ([]*BranchReport, Error) {
	brList, err := rs.Storage.GetAllBranchReport(r)
	if err != nil {
		return nil, NewErrors(500, `all_branch_report`, err.Error())
	}

	return brList, nil
}

func (rs *ReportServ) GetAllPartReport(r string) ([]*PartReport, Error) {
	prList, err := rs.Storage.GetAllPartReport(r)
	if err != nil {
		return nil, NewErrors(500, `all_part_report`, err.Error())
	}

	return prList, nil
}

func (rs *ReportServ) GetAllReport(r string) (*AllReport, Error) {
	ar, err := rs.Storage.GetAllReport(r)
	if err != nil {
		return nil, NewErrors(500, `all_report`, err.Error())
	}

	return ar, nil
}

func (rs *ReportServ) GetDialyReport(t time.Time) (*DialyReport, Error) {
	dr := DialyReport{
		Finished:    []*User{},
		NonRegister: make(map[string]string),
	}

	for k, v := range UserList {
		dr.NonRegister[k] = v
	}

	uList, err := rs.Storage.GetAllUser(GetCurrentRound())
	if err != nil {
		return nil, NewErrors(500, `dialy_report`, err.Error())
	}

	for _, u := range uList {
		loc, _ := time.LoadLocation(`Asia/Bangkok`)
		st := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, loc)
		et := time.Date(t.Year(), t.Month(), t.Day()+1, 0, 0, 0, 0, loc)
		if u.IsDone && st.Before(u.FinishedTime) && et.After(u.FinishedTime) {
			dr.Finished = append(dr.Finished, u)
		}

		if u.Department == `zetc` {
			dr.Etc++
		}

		if u.IsDone && u.IsPassed {
			dr.Passed++
		}
		if u.IsDone && !u.IsPassed {
			dr.Failed++
		}

		delete(dr.NonRegister, u.ID)
	}

	return &dr, nil
}

func (rs *ReportServ) CSVReport(name string) error {
	cr := GetCurrentRound()
	qs, err := GetQuestions(cr)
	if err != nil {
		return err
	}

	for kd, _ := range DepartmentList {
		f, err := os.Create(name + `_` + kd + `.csv`)
		if err != nil {
			return err
		}

		uList, err := rs.Storage.GetAllUserDepartment(kd, cr)
		if err != nil {
			return NewErrors(500, `csv_report`, err.Error())
		}
		rows := [][]string{}

		header := []string{
			`รหัส`,
			`ชื่อ - นามสกุล`,
			`ลักษณะงานหรือส่วนงานในปัจจุบัน`,
			`ฝ่าย`,
			`สำนัก`,
			`อายุการทำงานที่ธนาคารทั้งหมด`,
			`อายุการทำงานตำแหน่งปัจจุบัน`,
			`ประสบการณ์ด้านบัญชีเงินกู้`,
			`ประสบการณ์ด้านสินเชื่อ`,
			`ประสบการณ์ด้านเงินฝาก`,
			`ประสบการณ์ด้านเงินฝาก`,
			`ประสบการณ์ด้านบริหารหนี้`,
			`ประสบการณ์ด้านผู้ช่วย/ผู้จัดการสาขา`,
			`อื่นๆ`,
			`ประสบการณ์ด้านอื่นๆ`,
			`คะแนน`,
			`ผลการทดสอบ`,
			`เวลาที่ทดสอบเสร็จ`,
		}

		for _, j := range qs[kd].Functionals {
			header = append(header, j.Title)
		}

		for _, j := range qs[kd].Questions {
			header = append(header, j.Title)
		}

		rows = append(rows, header)

		for _, u := range uList {
			isPass := `ไม่ผ่าน`
			if u.IsPassed {
				isPass = `ผ่าน`
			}

			data := []string{
				u.ID,
				u.Name,
				DepartmentList[u.Department],
				u.Branch, u.Part, strconv.Itoa(u.WorkExp),
				strconv.Itoa(u.CurPosExp),
				strconv.Itoa(u.ActExp),
				strconv.Itoa(u.CrdExp),
				strconv.Itoa(u.FinExp),
				strconv.Itoa(u.LegExp),
				strconv.Itoa(u.DebtExp),
				strconv.Itoa(u.MgtExp),
				u.Other,
				strconv.Itoa(u.OtherExp),
				strconv.Itoa(u.Score),
				isPass,
				u.FinishedTime.Format(time.RFC3339),
			}

			for _, fun := range u.Functionals {
				data = append(data, strconv.FormatFloat(float64(fun.Score), 'f', 2, 64))
			}

			uaList, err := rs.Storage.GetUserAnswers(u.ID, cr)
			if err != nil {
				return err
			}
			for _, ua := range uaList {
				for _, q := range qs[kd].Questions {
					if q.ID == ua.QuestionID {
						data = append(data, q.Choices[ua.UserAnswer-1])
					}
				}
			}

			rows = append(rows, data)
		}

		cw := csv.NewWriter(f)
		cw.WriteAll(rows)
	}

	return nil
}
