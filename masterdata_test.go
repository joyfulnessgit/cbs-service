package service

import (
	"code.google.com/p/gomock/gomock"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
	"time"
)

func TestMasterData(t *testing.T) {
	Convey(`MasterData`, t, func() {
		ctrl := gomock.NewController(t)
		storage := NewMockStorage(ctrl)
		App.Storage = storage

		Reset(func() {
			ctrl.Finish()
		})

		Convey(`GetQuestions`, func() {
			Convey(`when has result`, func() {
				q, err := GetQuestions(`r1`)
				So(err, ShouldBeNil)
				So(q, ShouldNotEqual, Questions{})
			})
			Convey(`when has not result`, func() {
				q, err := GetQuestions(`r2`)
				So(err, ShouldNotBeNil)
				So(q, ShouldResemble, Questions{})
			})
		})

		Convey(`UserExist`, func() {
			Convey(`when user exist`, func() {
				e := UserExist(`00372`)
				So(e, ShouldBeTrue)
			})
			Convey(`when user doesn't exist`, func() {
				e := UserExist(`13656222`)
				So(e, ShouldBeFalse)
			})
		})

		Convey(`GetCurrentRound`, func() {
			Convey(`when get result`, func() {
				So(GetCurrentRound(), ShouldEqual, `r1`)
			})
		})

		Convey(`IsRoundExpired`, func() {
			Convey(`when get result`, func() {
				isExp := IsRoundExpired(`r1`)
				So(isExp, ShouldBeTrue)
			})
		})

		Convey(`GetRoundExpired`, func() {
			Convey(`when get result`, func() {
				t := GetRoundExpired(`r1`)
				loc, _ := time.LoadLocation(`Asia/Bangkok`)
				So(t.Format(time.RFC3339), ShouldEqual, time.Date(2014, 6, 20, 23, 59, 59, 0, loc).Format(time.RFC3339))
			})
		})

		Convey(`GetUserName`, func() {
			Convey(`when user exist`, func() {
				n := GetUserName(`00372`)
				So(n, ShouldEqual, `นางสาว กรภัค แสงมงคล`)
			})
			Convey(`when user not exist`, func() {
				n := GetUserName(`99999`)
				So(n, ShouldBeBlank)
			})
		})
	})
}
