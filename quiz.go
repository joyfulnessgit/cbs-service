package service

type QuizServ struct {
	Storage   Storage
	Validator *Validator
}

func NewQuizService() *QuizServ {
	return &QuizServ{App.Storage, NewValidator(App.Storage)}
}

func (qs *QuizServ) GetUserQuestion(userID string) (*QuestionsDept, map[string]*UserAnswer, Error) {
	u, err := qs.Storage.GetUser(userID, GetCurrentRound())
	if err != nil {
		return nil, nil, NewErrors(400, `quiz_getuserquestion`, err.Error())
	}

	q, err := qs.getQuestions(u.Round, u.Department)
	if err != nil {
		return nil, nil, NewErrors(400, `quiz_getuserquestion`, err.Error())
	}

	ua, err := qs.Storage.GetUserAnswers(u.ID, u.Round)
	if err != nil {
		return nil, nil, NewErrors(400, `quiz_getuserquestion`, err.Error())
	}

	return q, ua, nil
}

func (qs *QuizServ) getQuestions(r string, dept string) (*QuestionsDept, Error) {
	q, err := GetQuestions(r)
	if err != nil {
		return nil, err
	}

	if _, ok := q[dept]; !ok {
		return nil, NewErrors(404, `quiz_getquestions`, `question doesn't exist`)
	}

	qd := q[dept]
	return &qd, nil
}

func (qs *QuizServ) Answer(af *AnswerForm) Error {
	u, err := qs.Storage.GetUser(af.UserID, GetCurrentRound())
	if err != nil {
		return NewErrors(400, `answer`, err.Error())
	}

	ua := &UserAnswer{
		UserID:     u.ID,
		Round:      u.Round,
		QuestionID: af.QuestionID,
		UserAnswer: af.UserAnswer,
	}
	if err := qs.Storage.SaveAnswer(ua); err != nil {
		return NewErrors(500, `answer`, err.Error())
	}

	return nil
}

func (qs *QuizServ) SaveQuizProfile(uf *QuizProfileForm) (*User, Error) {

	qs.Validator.RequiredTH(uf.ID, `id`)
	qs.Validator.RequiredTH(uf.Name, `name`)
	qs.Validator.RequiredTH(uf.Department, `department`)
	qs.Validator.RequiredTH(uf.Branch, `branch`)
	qs.Validator.RequiredTH(uf.Part, `part`)
	qs.Validator.RequiredTH(uf.WorkExp, `workexp`)
	qs.Validator.RequiredTH(uf.CurPosExp, `curposexp`)
	qs.Validator.ExistUser(uf.ID, `id`)
	if uf.Department == `zetc` {
		qs.Validator.RequiredTH(uf.Other, `other`)
		qs.Validator.RequiredTH(uf.OtherExp, `other_exp`)
	}

	if qs.Validator.HasError() {
		return nil, NewValidateErrors(qs.Validator.GetFirstMessages())
	}

	cr := GetCurrentRound()
	if IsRoundExpired(cr) {
		return nil, NewErrors(400, `quiz_savequizprofile`, `quiz is expired`)
	}

	ru, err := qs.Storage.GetUser(uf.ID, cr)
	if err != nil && err.Error() != `not found` {
		return nil, NewErrors(400, `quiz_savequizprofile`, err.Error())
	}
	if ru != nil && err == nil {
		if ru.IsDone {
			return nil, NewErrors(400, `quiz_savequizprofile`, `user is already finished quiz`)
		} else {
			return nil, NewErrors(400, `quiz_savequizprofile`, `user is already registered but not completed`)
		}
	}

	var totalScore int
	if uf.Department != `zetc` {
		q, err := qs.getQuestions(cr, uf.Department)
		if err != nil {
			return nil, NewErrors(500, `quiz_savequizprofile`, err.Error())
		}
		totalScore = len(q.Questions)
	}

	u := User{
		ID:         uf.ID,
		Round:      cr,
		Name:       uf.Name,
		Department: uf.Department,
		Branch:     uf.Branch,
		Part:       uf.Part,
		WorkExp:    uf.WorkExp,
		CurPosExp:  uf.CurPosExp,
		ActExp:     uf.ActExp,
		CrdExp:     uf.CrdExp,
		FinExp:     uf.FinExp,
		LegExp:     uf.LegExp,
		DebtExp:    uf.DebtExp,
		MgtExp:     uf.MgtExp,
		Score:      0,
		TotalScore: totalScore,
		IsPassed:   false,
		IsDone:     false,
		Other:      uf.Other,
		OtherExp:   uf.OtherExp,
	}

	if err := qs.Storage.SaveUser(&u); err != nil {
		return nil, NewErrors(500, `quiz_savequizprofile`, err.Error())
	}

	return &u, nil
}
