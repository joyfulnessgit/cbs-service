package service

import (
	"code.google.com/p/gomock/gomock"
	"errors"
	. "github.com/smartystreets/goconvey/convey"
	"strconv"
	"testing"
	"time"
)

func getUAList(an int, userID string, r string) map[string]*UserAnswer {
	uaList := map[string]*UserAnswer{}
	var x int
	for i := 1; i <= 15; i++ {

		qid := `r1actq`
		if an == 0 {
			switch i {
			case 1, 3, 10:
				x = 1
			case 7, 12, 13, 14, 15:
				x = 2
			case 4, 6, 9, 11:
				x = 3
			case 2, 5, 8:
				x = 4
			}
		} else {
			x = an
		}
		uaList[qid+strconv.Itoa(i)] = &UserAnswer{
			UserID:     userID,
			Round:      r,
			QuestionID: qid + strconv.Itoa(i),
			UserAnswer: x,
		}
	}

	return uaList
}

func getFunctionalR1() map[string]*Functional {
	return map[string]*Functional{
		`r1actf1`: &Functional{
			Title:      `ความรู้เรื่อง Statement เงินกู้และการคำนวณดอกเบี้ยโดย manual`,
			TotalScore: 4,
		},
		`r1actf2`: &Functional{
			Title:      `ความรู้เรื่องบัญชี Proxy`,
			TotalScore: 3,
		},
		`r1actf3`: &Functional{
			Title:      `ความรู้เรื่องบัญชี Account Balance Mismatch`,
			TotalScore: 3,
		},
		`r1actf4`: &Functional{
			Title:      `ความรู้เรื่องการ Post ดอกเบี้ย`,
			TotalScore: 2,
		},
		`r1actf5`: &Functional{
			Title:      `ความรู้เรื่องการบันทึกบัญชีเงินกู้`,
			TotalScore: 3,
		},
	}
}

func TestReport(t *testing.T) {
	now := time.Now().Truncate(time.Second)

	Convey(`ReportService`, t, func() {
		ctrl := gomock.NewController(t)
		storage := NewMockStorage(ctrl)

		App.Storage = storage
		rs := NewReportService()
		rs.Now = func() time.Time { return now }

		Reset(func() {
			ctrl.Finish()
		})

		Convey(`CreateUserReport`, func() {
			userID := `24497`
			cr := `r1`
			u := User{
				ID:         userID,
				Round:      cr,
				Department: `act`,
				Branch:     `สาขาพระราม 6`,
				Score:      0,
				TotalScore: 15,
				IsPassed:   false,
				IsDone:     false,
			}

			Convey(`when user create report more than 1 time`, func() {
				au := u
				au.IsDone = true
				storage.EXPECT().GetUser(userID, cr).Return(&au, nil)
				err := rs.CreateUserReport(userID)
				So(err, ShouldBeNil)
			})

			Convey(`when first user finished quiz`, func() {
				eu := u
				eu.Score = 15
				eu.IsPassed = true
				eu.FinishedTime = now
				eu.IsDone = true

				uaList := getUAList(0, userID, cr)

				eufs := getFunctionalR1()
				eufs[`r1actf1`].Score = 4
				eufs[`r1actf1`].UserScore = 4
				eufs[`r1actf2`].Score = 3
				eufs[`r1actf2`].UserScore = 3
				eufs[`r1actf3`].Score = 3
				eufs[`r1actf3`].UserScore = 3
				eufs[`r1actf4`].Score = 2
				eufs[`r1actf4`].UserScore = 2
				eufs[`r1actf5`].Score = 3
				eufs[`r1actf5`].UserScore = 3

				eu.Functionals = eufs

				storage.EXPECT().GetUser(userID, cr).Return(&u, nil)
				storage.EXPECT().GetUserAnswers(userID, cr).Return(uaList, nil)

				storage.EXPECT().SaveUser(&eu).Return(nil)

				err := rs.CreateUserReport(userID)
				So(err, ShouldBeNil)
			})

			Convey(`when 2 user finished quiz same branch and department`, func() {
				eu := u
				eu.Score = 3
				eu.IsPassed = false
				eu.FinishedTime = now
				eu.IsDone = true
				eufs := getFunctionalR1()
				eufs[`r1actf1`].Score = 1
				eufs[`r1actf1`].UserScore = 1
				eufs[`r1actf2`].Score = 1
				eufs[`r1actf2`].UserScore = 1
				eufs[`r1actf3`].Score = 1
				eufs[`r1actf3`].UserScore = 1
				eu.Functionals = eufs

				uaList := getUAList(4, userID, cr)

				storage.EXPECT().GetUser(userID, cr).Return(&u, nil)
				storage.EXPECT().GetUserAnswers(userID, cr).Return(uaList, nil)

				storage.EXPECT().SaveUser(&eu).Return(nil)

				err := rs.CreateUserReport(userID)
				So(err, ShouldBeNil)
			})
		})
		Convey(`GetUserReport`, func() {
			userID := `24497`
			cr := `r1`
			eu := User{}

			Convey(`when get result`, func() {
				storage.EXPECT().GetUser(userID, cr).Return(&eu, nil)
				u, err := rs.GetUserReport(userID, cr)
				So(err, ShouldBeNil)
				So(u, ShouldResemble, &eu)
			})
			Convey(`when db error`, func() {
				storage.EXPECT().GetUser(userID, cr).Return(nil, errors.New(`db error`))
				u, err := rs.GetUserReport(userID, cr)
				So(err, ShouldResemble, NewErrors(500, `user_report`, `db error`))
				So(u, ShouldBeNil)
			})
		})
		Convey(`GetAllDepartmentReport`, func() {
			cr := `r1`

			edr := map[string]*DepartmentReport{}

			Convey(`when get result`, func() {
				storage.EXPECT().GetDepartmentReports(cr).Return(edr, nil)
				dr, err := rs.GetAllDepartmentReport(cr)
				So(err, ShouldBeNil)
				So(dr, ShouldResemble, edr)
			})

			Convey(`when db error`, func() {
				storage.EXPECT().GetDepartmentReports(cr).Return(nil, errors.New(`db error`))
				dr, err := rs.GetAllDepartmentReport(cr)
				So(err, ShouldResemble, NewErrors(500, `department_report`, `db error`))
				So(dr, ShouldBeNil)
			})
		})
		Convey(`GetBranchReport`, func() {
			cr := `r1`
			b := `test_branch`
			ebr := &BranchReport{}
			Convey(`when get result`, func() {
				storage.EXPECT().GetBranchReport(b, cr).Return(ebr, nil)
				br, err := rs.GetBranchReport(b, cr)
				So(err, ShouldBeNil)
				So(br, ShouldResemble, ebr)
			})

			Convey(`when db error`, func() {
				storage.EXPECT().GetBranchReport(b, cr).Return(nil, errors.New(`db error`))
				br, err := rs.GetBranchReport(b, cr)
				So(err, ShouldResemble, NewErrors(500, `branch_report`, `db error`))
				So(br, ShouldBeNil)
			})
		})

		Convey(`GetAllReport`, func() {
			cr := `r1`
			ear := &AllReport{}

			Convey(`when get result`, func() {
				storage.EXPECT().GetAllReport(cr).Return(ear, nil)
				ar, err := rs.GetAllReport(cr)
				So(err, ShouldBeNil)
				So(ar, ShouldResemble, ear)
			})

			Convey(`when db error`, func() {
				storage.EXPECT().GetAllReport(cr).Return(nil, errors.New(`db error`))
				ar, err := rs.GetAllReport(cr)
				So(err, ShouldResemble, NewErrors(500, `all_report`, `db error`))
				So(ar, ShouldBeNil)
			})
		})

		Convey(`GetAllBranchReport`, func() {
			cr := `r1`
			ebr := []*BranchReport{}
			Convey(`when get result`, func() {
				storage.EXPECT().GetAllBranchReport(cr).Return(ebr, nil)
				br, err := rs.GetAllBranchReport(cr)
				So(err, ShouldBeNil)
				So(br, ShouldResemble, ebr)
			})

			Convey(`when db error`, func() {
				storage.EXPECT().GetAllBranchReport(cr).Return(nil, errors.New(`db error`))
				br, err := rs.GetAllBranchReport(cr)
				So(err, ShouldResemble, NewErrors(500, `all_branch_report`, `db error`))
				So(br, ShouldBeNil)
			})
		})

		Convey(`GetDialyReport`, func() {
			cr := `r1`
			uList := []*User{}

			loc, _ := time.LoadLocation(`Asia/Bangkok`)
			now := time.Now()
			tt := time.Date(now.Year(), now.Month(), now.Day(), now.Hour(), now.Minute(), now.Second(), 0, loc)
			u := &User{
				ID:           `111`,
				Round:        cr,
				Department:   `act`,
				Branch:       `สาขาพระราม 6`,
				Score:        15,
				TotalScore:   15,
				IsPassed:     true,
				IsDone:       true,
				FinishedTime: tt,
			}

			uList = append(uList, u)

			Convey(`when get result`, func() {
				storage.EXPECT().GetAllUser(cr).Return(uList, nil)
				dr, err := rs.GetDialyReport(time.Now())
				So(err, ShouldBeNil)
				So(dr.Passed, ShouldEqual, 1)
				So(dr.Failed, ShouldEqual, 0)
				So(dr.Etc, ShouldEqual, 0)
				So(dr.Finished, ShouldResemble, uList)
			})

			Convey(`when db error`, func() {
				storage.EXPECT().GetAllUser(cr).Return(nil, errors.New(`db error`))
				dr, err := rs.GetDialyReport(tt)
				So(err, ShouldResemble, NewErrors(500, `dialy_report`, `db error`))
				So(dr, ShouldBeNil)
			})
		})

		// Convey(`GenerateReport`, func() {
		// 	cr := `r1`
		// 	u := User{
		// 		Round:      cr,
		// 		Department: `act`,
		// 		Branch:     `สาขาพระราม 6`,
		// 		Score:      0,
		// 		TotalScore: 15,
		// 		IsPassed:   false,
		// 		IsDone:     false,
		// 	}

		// 	Convey(`when successful`, func() {
		// 		eUList := []*User{}

		// 		eu1 := u
		// 		eu1.ID = `1`
		// 		eu1.Score = 5
		// 		eUList = append(eUList, &eu1)

		// 		eu2 := u
		// 		eu2.ID = `2`
		// 		eu2.IsPassed = true
		// 		eu2.Score = 13
		// 		eUList = append(eUList, &eu2)

		// 		fmt.Println(eUList[0].IsPassed)
		// 		fmt.Println(eUList[1].IsPassed)

		// 		eUfList := map[string]*UserFunctional{
		// 			`1`: &UserFunctional{},
		// 			`2`: &UserFunctional{},
		// 		}
		// 		storage.EXPECT().GetAllUserDoneCurrentRound(cr).Return(eUList, nil)
		// 		storage.EXPECT().GetCurrentRoundAllUserFunctional(cr).Return(eUfList, nil)
		// 		err := rs.GenerateReport()
		// 		So(err, ShouldBeNil)
		// 	})
		// })
	})
}
