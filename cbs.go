package service

import (
	"os"
	"time"
)

var App *CBS
var dataPath string

type CBS struct {
	Storage Storage
}

func init() {
	App = &CBS{}
	dataPath = os.Getenv(`CBS_DATA_PATH`)
	loadMasterData()
}

type Storage interface {
	GetUser(id string, r string) (*User, error)
	GetAllUserDepartment(d string, r string) ([]*User, error)
	GetAllUserDoneCurrentRound(r string) ([]*User, error)
	GetAllUser(r string) ([]*User, error)
	SaveUser(u *User) error
	SaveAnswer(ua *UserAnswer) error
	GetUserAnswers(userID string, r string) (map[string]*UserAnswer, error)
	GetUserDoneByDate(t time.Time, r string) ([]*User, error)

	SaveAllReport(ar *AllReport) error
	GetAllReport(r string) (*AllReport, error)
	SaveDepartmentReport(dr *DepartmentReport) error
	GetDepartmentReports(r string) (map[string]*DepartmentReport, error)
	SaveBranchReport(br *BranchReport) error
	GetBranchReport(p string, b string, r string) (*BranchReport, error)
	GetAllBranchReport(r string) ([]*BranchReport, error)
	ClearReport(r string) error

	SavePartReport(pr *PartReport) error
	GetPartReport(p string, r string) (*PartReport, error)
	GetAllPartReport(r string) ([]*PartReport, error)
}
