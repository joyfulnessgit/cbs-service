package service

import (
	"bitbucket.org/plimble/validator"
	"encoding/json"
)

func NewErrors(sc int, t string, msg string) Error {
	return &BaseError{sc, t, msg}
}

func NewStorageErrors(msg string) Error {
	if msg == `not found` {
		return &BaseError{404, `not_found`, `resource not found`}
	}

	return &BaseError{500, `storage`, msg}
}

func NewValidateErrors(msg validator.FirstMessage) Error {
	return &BaseError{400, `validate`, msg}
}

func NewNotFoundErrors(msg string) Error {
	if msg == `` {
		msg = `resource not found`
	}
	return &BaseError{404, `not_found`, msg}
}

type BaseError struct {
	StatusCode int         `json:"-"`
	Type       string      `json:"type"`
	Message    interface{} `json:"message"`
}

type Error interface {
	Error() string
	GetType() string
	GetStatusCode() int
	GetMessage() interface{}
	IsValidationType() bool
}

func (this *BaseError) Error() string {
	switch this.Message.(type) {
	case string:
		return this.Type + `: ` + this.Message.(string)
	case validator.FirstMessage:
		v, _ := json.Marshal(this.Message)
		return this.Type + `: ` + string(v)
	}

	return ``
}

func (this *BaseError) GetType() string {
	return this.Type
}

func (this *BaseError) GetMessage() interface{} {
	return this.Message
}

func (this *BaseError) GetStatusCode() int {
	return this.StatusCode
}

func (this *BaseError) IsValidationType() bool {
	if this.Type != `validate` {
		return false
	}

	return true
}
