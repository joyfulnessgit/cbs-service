package service

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"time"
)

var QuestionList map[string]Questions
var UserList map[string]string
var RoundList Rounds
var BranchList map[string][]string
var DepartmentList map[string]string

func loadMasterData() {
	QuestionList = make(map[string]Questions)
	if err := getBranchList(); err != nil {
		panic(err)
	}
	if err := getUserList(); err != nil {
		panic(err)
	}
	if err := getRoundList(); err != nil {
		panic(err)
	}

	DepartmentList = map[string]string{
		"crd":  "สินเชื่อ",
		"leg":  "นิติกรรม",
		"fin":  "การเงิน",
		"act":  "บัญชีเงินกู้",
		"debt": "บริหารหนี้",
		"mgt":  "ผู้ช่วย/ผู้จัดการสาขา",
		"zetc": "อื่นๆ",
	}
}

func GetQuestions(r string) (Questions, Error) {
	qr := Questions{}
	if _, ok := QuestionList[r]; !ok {
		file, err := ioutil.ReadFile(dataPath + `/questions/` + r + `.json`)
		if err != nil {
			return qr, NewErrors(500, `question_list`, err.Error())
		}
		if err := json.Unmarshal(file, &qr); err != nil {
			return qr, NewErrors(500, `question_list`, err.Error())
		}
		QuestionList[r] = qr
	}

	return QuestionList[r], nil
}

func UserExist(userID string) bool {
	if _, ok := UserList[userID]; !ok {
		return false
	}

	return true
}

//return round, expired
func GetCurrentRound() string {
	return RoundList.Current
}

func GetUserName(userID string) string {
	if UserExist(userID) {
		return UserList[userID]
	}

	return ""
}

func GetRoundExpired(r string) time.Time {
	t, _ := time.Parse(time.RFC3339, RoundList.Rounds[RoundList.Current].Expired)
	return t
}

func IsRoundExpired(r string) bool {
	isExp := false
	expStr := RoundList.Rounds[RoundList.Current].Expired

	exp, _ := time.Parse(time.RFC3339, expStr)

	if exp.UTC().Before(time.Now().UTC()) {
		isExp = true
	}
	return isExp
}

func getBranchList() error {
	file, err := ioutil.ReadFile(dataPath + `/branch.json`)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(file, &BranchList); err != nil {
		return err
	}

	return nil
}

func getUserList() error {
	file, err := ioutil.ReadFile(dataPath + `/user.json`)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(file, &UserList); err != nil {
		return err
	}

	return nil
}

func getRoundList() error {
	file, err := ioutil.ReadFile(dataPath + `/round.json`)
	if err != nil {
		return err
	}

	if err := json.Unmarshal(file, &RoundList); err != nil {
		return err
	}

	if RoundList.Current == `` {
		return errors.New(`no current round in json`)
	}

	if _, ok := RoundList.Rounds[RoundList.Current]; !ok {
		return errors.New(`no current round in json list`)
	}

	return nil
}
