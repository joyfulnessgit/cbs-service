package service

import (
	"time"
)

type UserServ struct {
	Storage Storage
}

func NewUserService() *UserServ {
	return &UserServ{App.Storage}
}

func (us *UserServ) GetCurrentRoundUser(user_id string) (*User, Error) {
	u, err := us.Storage.GetUser(user_id, GetCurrentRound())
	if err != nil {
		return nil, NewErrors(400, `user_getuser`, err.Error())
	}

	return u, nil
}

func (us *UserServ) GetDoneByDate(t time.Time) ([]*User, Error) {
	uArr, err := us.Storage.GetUserDoneByDate(t, GetCurrentRound())
	if err != nil {
		return uArr, NewErrors(500, `get_done_date`, err.Error())
	}

	return uArr, nil
}
