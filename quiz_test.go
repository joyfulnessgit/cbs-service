package service

import (
	"bitbucket.org/plimble/validator"
	"code.google.com/p/gomock/gomock"
	"errors"
	. "github.com/smartystreets/goconvey/convey"
	"testing"
)

func TestQuiz(t *testing.T) {

	Convey(`QuizService`, t, func() {
		ctrl := gomock.NewController(t)
		storage := NewMockStorage(ctrl)

		App.Storage = storage
		qs := NewQuizService()
		Reset(func() {
			ctrl.Finish()
		})

		Convey(`SaveQuizProfile`, func() {
			RoundList.Rounds[`r1`].Expired = "2020-06-20T23:59:59+07:00"
			qp := &QuizProfileForm{
				ID:         `00372`,
				Name:       "นางสาว กรภัค แสงมงคล",
				Department: "crd",
				Branch:     "สาขาเทส",
				Part:       "กทม.และปริมณฑล 1",
				WorkExp:    10,
				CurPosExp:  10,
				ActExp:     10,
				CrdExp:     10,
				FinExp:     10,
				LegExp:     10,
			}

			Convey(`when save successful`, func() {
				storage.EXPECT().GetUser(`00372`, `r1`).Return(nil, errors.New(`not found`))
				storage.EXPECT().SaveUser(gomock.Any()).Return(nil)

				au := &User{
					ID:         `00372`,
					Name:       "นางสาว กรภัค แสงมงคล",
					Department: "crd",
					Branch:     "สาขาเทส",
					Part:       "กทม.และปริมณฑล 1",
					WorkExp:    10,
					CurPosExp:  10,
					ActExp:     10,
					CrdExp:     10,
					FinExp:     10,
					LegExp:     10,
					Round:      RoundList.Current,
					Score:      0,
					TotalScore: 15,
					IsPassed:   false,
					IsDone:     false,
				}

				u, err := qs.SaveQuizProfile(qp)
				So(err, ShouldBeNil)
				So(u, ShouldResemble, au)
			})
			Convey(`when user is exist and finished quiz`, func() {
				u := &User{
					ID:     `00372`,
					IsDone: true,
				}
				storage.EXPECT().GetUser(`00372`, `r1`).Return(u, nil)
				u, err := qs.SaveQuizProfile(qp)
				So(err, ShouldResemble, NewErrors(400, `quiz_savequizprofile`, `user is already finished quiz`))
				So(u, ShouldBeNil)
			})

			Convey(`when user is exist and quiz is not completed`, func() {
				u := &User{
					ID:     `00372`,
					IsDone: false,
				}
				storage.EXPECT().GetUser(`00372`, `r1`).Return(u, nil)
				u, err := qs.SaveQuizProfile(qp)
				So(err, ShouldResemble, NewErrors(400, `quiz_savequizprofile`, `user is already registered but not completed`))
				So(u, ShouldBeNil)
			})

			Convey(`when db error`, func() {
				storage.EXPECT().GetUser(`00372`, `r1`).Return(nil, errors.New(`not found`))
				storage.EXPECT().SaveUser(gomock.Any()).Return(errors.New(`error db`))
				u, err := qs.SaveQuizProfile(qp)
				So(err, ShouldResemble, NewErrors(500, `quiz_savequizprofile`, `error db`))
				So(u, ShouldBeNil)
			})

			Convey(`when quiz is already ended`, func() {
				RoundList.Rounds[RoundList.Current].Expired = `2014-05-26T00:00:00+07:00`
				u, err := qs.SaveQuizProfile(qp)
				So(err, ShouldResemble, NewErrors(400, `quiz_savequizprofile`, `quiz is expired`))
				So(u, ShouldBeNil)
				RoundList.Rounds[RoundList.Current].Expired = `2014-08-01T23:59:59+07:00`
			})

			Convey(`when user not found`, func() {
				newqp := *qp
				newqp.ID = `1111`
				u, err := qs.SaveQuizProfile(&newqp)
				So(err, ShouldResemble, NewValidateErrors(validator.FirstMessage{
					`id`: `ไม่พบรหัสพนักงาน`,
				}))
				So(u, ShouldBeNil)
			})

			Convey(`when validate error`, func() {
				qp := &QuizProfileForm{}
				u, err := qs.SaveQuizProfile(qp)
				So(u, ShouldBeNil)
				So(err, ShouldResemble, NewValidateErrors(validator.FirstMessage{
					`branch`:     `กรุณาเลือก`,
					`name`:       `กรุณากรอก`,
					`department`: `กรุณาเลือก`,
					`id`:         `กรุณากรอก`,
					`curposexp`:  `กรุณาเลือก`,
					`workexp`:    `กรุณาเลือก`,
					`part`:       `กรุณาเลือก`,
				}))
			})

			Convey(`when validate error with department is etc`, func() {
				qp := &QuizProfileForm{}
				qp.Department = `zetc`
				u, err := qs.SaveQuizProfile(qp)
				So(u, ShouldBeNil)
				So(err, ShouldResemble, NewValidateErrors(validator.FirstMessage{
					`branch`:    `กรุณาเลือก`,
					`name`:      `กรุณากรอก`,
					`id`:        `กรุณากรอก`,
					`curposexp`: `กรุณาเลือก`,
					`workexp`:   `กรุณาเลือก`,
					`part`:      `กรุณาเลือก`,
					`other`:     `กรุณากรอก`,
					`other_exp`: `กรุณาเลือก`,
				}))
			})
		})

		Convey(`Answer`, func() {
			af := &AnswerForm{
				UserID:     `00372`,
				QuestionID: `r1actq1`,
				UserAnswer: 1,
			}
			ua := &UserAnswer{
				UserID:     `00372`,
				Round:      `r1`,
				QuestionID: `r1actq1`,
				UserAnswer: 1,
			}
			u := &User{
				ID:     `00372`,
				Round:  `r1`,
				IsDone: false,
			}

			Convey(`when successful`, func() {
				storage.EXPECT().GetUser(`00372`, `r1`).Return(u, nil)
				storage.EXPECT().SaveAnswer(ua).Return(nil)
				err := qs.Answer(af)
				So(err, ShouldBeNil)
			})

			Convey(`when user answer last question`, func() {
				u := &User{
					ID:    `00372`,
					Round: `r1`,
				}
				storage.EXPECT().GetUser(`00372`, `r1`).Return(u, nil)
				storage.EXPECT().SaveAnswer(ua).Return(nil)
				err := qs.Answer(af)
				So(err, ShouldBeNil)
			})
			Convey(`when get user error`, func() {
				storage.EXPECT().GetUser(`00372`, `r1`).Return(nil, errors.New(`db error`))
				err := qs.Answer(af)
				So(err, ShouldResemble, NewErrors(400, `answer`, `db error`))
			})
			Convey(`when save answer error`, func() {
				storage.EXPECT().GetUser(`00372`, `r1`).Return(u, nil)
				storage.EXPECT().SaveAnswer(ua).Return(errors.New(`db error`))
				err := qs.Answer(af)
				So(err, ShouldResemble, NewErrors(500, `answer`, `db error`))
			})
		})

	})
}
