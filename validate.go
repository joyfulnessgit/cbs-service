package service

import (
	"bitbucket.org/plimble/validator"
)

type Validator struct {
	*validator.Validator
	Storage Storage
}

func NewValidator(s Storage) *Validator {
	return &Validator{validator.NewValidaor(), s}
}

func (v *Validator) RequiredTH(val interface{}, f string) {
	switch f {
	case "department", "part", "branch", "workexp", "curposexp", "other_exp":
		v.Required(val, f).Message(`กรุณาเลือก`)
	default:
		v.Required(val, f).Message(`กรุณากรอก`)
	}
}

func (v *Validator) ExistUser(val string, f string) error {
	msg := validator.NewMessage(f, false, `ไม่พบรหัสพนักงาน`)
	if !UserExist(val) {
		msg.IsError = true
	}
	v.AddError(msg)
	return nil
}
