package service

import (
	"time"
)

type User struct {
	ID           string                 `json:"id" bson:"id"`
	Round        string                 `json:"round" bson:"round"`
	Name         string                 `json:"name" bson:"name"`
	Department   string                 `json:"department" bson:"department"`
	Branch       string                 `json:"branch" bson:"branch"`
	Part         string                 `json:"part" bson:"part"`
	WorkExp      int                    `json:"work_exp" bson:"work_exp"`
	CurPosExp    int                    `json:"cur_pos_exp" bson:"cur_pos_exp"`
	ActExp       int                    `json:"act_exp" bson:"act_exp"`
	CrdExp       int                    `json:"crd_exp" bson:"crd_exp"`
	FinExp       int                    `json:"fin_exp" bson:"fin_exp"`
	LegExp       int                    `json:"leg_exp" bson:"leg_exp"`
	DebtExp      int                    `json:"debt_exp" bson:"debt_exp"`
	MgtExp       int                    `json:"mgt_exp" bson:"mgt_exp"`
	Score        int                    `json:"score" bson:"score"`
	TotalScore   int                    `json:"total_score" bson:"total_score"`
	IsPassed     bool                   `json:"is_passed" bson:"is_passed"`
	IsDone       bool                   `json:"is_done" bson:"is_done"`
	FinishedTime time.Time              `json:"finished_time" bson:"finished_time"`
	Other        string                 `json:"other" bson:"other"`
	OtherExp     int                    `json:"other_exp" bson:"other_exp"`
	Functionals  map[string]*Functional `json:"functionals" bson:"functionals"`
}

type UserAnswer struct {
	UserID     string `json:"user_id" bson:"user_id"`
	Round      string `json:"round" bson:"round"`
	QuestionID string `json:"question_id" bson:"question_id"`
	UserAnswer int    `json:"user_answer" bson:"user_answer"`
}

type DialyReport struct {
	Finished    []*User           `json:"finished" bson:"finished"`
	NonRegister map[string]string `json:"non_register" bson:"non_register"`
	Etc         int               `json:"etc" bson:"etc"`
	Passed      int               `json:"passed" bson:"passed"`
	Failed      int               `json:"failed" bson:"failed"`
}

type AllReport struct {
	Round  string  `json:"round" bson:"round"`
	Report *Report `json:"report" bson:"report"`
}

type BranchReport struct {
	Part              string                       `json:"part" bson:"part"`
	Branch            string                       `json:"branch" bson:"branch"`
	Round             string                       `json:"round" bson:"round"`
	TotalUser         int                          `json:"total_user" bson:"total_user"`
	DepartmentReports map[string]*DepartmentReport `json:"department_reports" bson:"department_reports"`
}

type DepartmentReport struct {
	Department string  `json:"department" bson:"department"`
	Round      string  `json:"round" bson:"round"`
	Report     *Report `json:"report" bson:"report"`
}

type PartReport struct {
	Part              string                       `json:"part" bson:"part"`
	Round             string                       `json:"round" bson:"round"`
	TotalUser         int                          `json:"total_user" bson:"total_user"`
	DepartmentReports map[string]*DepartmentReport `json:"department_reports" bson:"department_reports"`
}

type Report struct {
	TotalUser   int                    `json:"total_user" bson:"total_user"`
	Functionals map[string]*Functional `json:"functionals" bson:"functionals"`
	Passed      int                    `json:"passed" bson:"passed"`
	Failed      int                    `json:"failed" bson:"failed"`
	MeanScore   float32                `json:"mean_score" bson:"mean_score"`
	MaxScore    int                    `json:"max_score" bson:"max_score"`
	MinScore    int                    `json:"min_score" bson:"min_score"`
	UserScore   int                    `json:"user_score" bson:"user_score"`
	TotalScore  int                    `json:"total_score" bson:"total_score"`
}

type Functional struct {
	Title      string  `json:"title" bson:"title"`
	Score      float32 `json:"score" bson:"score"`
	UserScore  int     `json:"user_score" bson:"user_score"`
	TotalScore int     `json:"total_score" bson:"total_score"`
}

type Questions map[string]QuestionsDept

type QuestionsDept struct {
	Header      string                `json:"header" bson:"header"`
	Info        []string              `json:"info" bson:"info"`
	Functionals map[string]Functional `json:"functionals" bson:"functionals"`
	TotalScore  int                   `json:"total_score" bson:"total_score"`
	Questions   []Question            `json:"questions" bson:"questions"`
}

type Question struct {
	ID         string   `json:"id" bson:"id"`
	Title      string   `json:"title" bson:"title"`
	Choices    []string `json:"choices" bson:"choices"`
	Answer     int      `json:"answer" bson:"answer"`
	Functional string   `json:"functional" bson:"functional"`
}

type Rounds struct {
	Current string                `json:"current"`
	Rounds  map[string]*RoundInfo `json:"rounds"`
}

type RoundInfo struct {
	Expired string `json:"expired"`
	Started string `json:"started"`
	Name    string `json:"name"`
}

type UserAnswerQuestion struct {
	Title      string
	Choices    []string
	Answer     int
	UserAnswer int
}
