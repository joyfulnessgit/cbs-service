package service

type QuizProfileForm struct {
	ID         string `param:"id"`
	Name       string `param:"name"`
	Department string `param:"department"`
	Branch     string `param:"branch"`
	Part       string `param:"part"`
	WorkExp    int    `param:"work_exp"`
	CurPosExp  int    `param:"curpos_exp"`
	ActExp     int    `param:"act_exp"`
	CrdExp     int    `param:"crd_exp"`
	FinExp     int    `param:"fin_exp"`
	LegExp     int    `param:"leg_exp"`
	DebtExp    int    `param:"debt_exp"`
	MgtExp     int    `param:"mgt_exp"`
	Other      string `param:"other"`
	OtherExp   int    `param:"other_exp"`
}

type ResumeQuizForm struct {
	UserID string `param:"user_id"`
}

type AnswerForm struct {
	UserID     string `param:"user_id"`
	QuestionID string `param:"question_id"`
	UserAnswer int    `param:"user_answer"`
}
